package com.base.redis.client;

import com.base.redis.model.Circle;
import com.base.redis.model.Location;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name="location", url = "localhost:8081")
public interface LocationClient {
    @PostMapping("fetch")
    List<Location> fetchData(@RequestBody Circle circle);

    @GetMapping("fetch-all")
    List<Location> fetchAllData();
}
