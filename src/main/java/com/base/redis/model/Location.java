package com.base.redis.model;

import lombok.*;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.GeoIndexed;

@RedisHash("Location")
@Data
@Builder(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Location {
    private String id;
    @GeoIndexed
    private Point geoLocation;
    private String name;
}
