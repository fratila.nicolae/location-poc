package com.base.redis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.geo.Point;

@Data
@AllArgsConstructor
public class Circle {
    private Point point;
    private long distance;
}