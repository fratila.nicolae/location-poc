package com.base.redis.repo;

import com.base.redis.model.Location;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepo extends CrudRepository<Location, String> {
    List<Location> findByGeoLocationNear(Point point, Distance distance);
}
