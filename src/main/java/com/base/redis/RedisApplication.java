package com.base.redis;

import com.base.redis.model.Circle;
import com.base.redis.client.LocationClient;
import com.base.redis.model.Location;
import com.base.redis.repo.LocationRepo;
import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.domain.geo.Metrics;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@SpringBootApplication
@RestController
@AllArgsConstructor
@EnableFeignClients
public class RedisApplication {

    public static final Point POINT = new Point(0, 0);
    public static final int DISTANCE = 20000000;

    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }

    private LocationRepo locationRepo;
    private LocationClient locationClient;

    @GetMapping("redis/fetch")
    public List<Location> fetchRedisData() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Fetch from redis");
        List<Location> locations = locationRepo.findByGeoLocationNear(POINT, new Distance(DISTANCE, Metrics.METERS));
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        System.out.println(stopWatch.getTotalTimeMillis());
        return locations;
    }

    @GetMapping("redis/fetch-all")
    public List<Location> fetchAllRedisData() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Fetch all from redis");
        List<Location> locations = StreamSupport.stream(locationRepo.findAll().spliterator(), false).collect(Collectors.toList());
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        System.out.println(stopWatch.getTotalTimeMillis());
        return locations;
    }

    @GetMapping("java/fetch")
    public List<Location> fetchJavaData() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Fetch from java");
        List<Location> locations = locationClient.fetchData(new Circle(new Point(0, 0), 20000000));
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        System.out.println(stopWatch.getTotalTimeMillis());
        return locations;
    }

    @GetMapping("java/fetch-all")
    public List<Location> fetchAllJavaData() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Fetch all from java");
        List<Location> locations = locationClient.fetchAllData();
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        System.out.println(stopWatch.getTotalTimeMillis());
        return locations;
    }

    @GetMapping("generate") // used to generate data in redis (also removes the data prior to generation to make sure the same data is used in both scenarios)
    public void generateData() {
        locationRepo.deleteAll();
        int i = 0;
        Random random = new Random(69);
        List<Location> locations = new Vector<>();
        while (i < 100000) {
            Location location = Location.builder()
                    .id("" + i)
                    .geoLocation(new Point(random.nextDouble() * 360 - 180, random.nextDouble() * 90 - 45))
                    .name("random name" + i).build();
            locations.add(location);
            i++;
        }
        locationRepo.saveAll(locations);
    }

    @GetMapping("diff") // used to make sure the same information is in both the app with the in-memory storage and redis
    public void checkDifferences() {
        List<Location> allLocationsJava = locationClient.fetchAllData();
        List<Location> allLocationsRedis = StreamSupport.stream(locationRepo.findAll().spliterator(), false).toList();
        if (!allLocationsJava.containsAll(allLocationsRedis) || !allLocationsRedis.containsAll(allLocationsJava))
            throw new AssertionError();
        Circle circle = new Circle(POINT, DISTANCE);
        List<Location> locationsJava = locationClient.fetchData(circle);
        List<Location> locationsRedis = locationRepo.findByGeoLocationNear(POINT, new Distance(DISTANCE, Metrics.METERS));
        if (!locationsJava.containsAll(locationsRedis) || !locationsRedis.containsAll(locationsJava))
            throw new AssertionError();
    }
}
